const { UserRepository } = require('../repositories/userRepository');

class UserService {

  // TODO: Implement methods to work with user
  create(data) {
    const search = this.search({'email': data.email});
    if (search)
        throw new Error(`User with email ${data.email} already extists!`);
    return UserRepository.create(data);
  }

  update(id, dataToUpdate) {
    return UserRepository.update(id, dataToUpdate);
  }

  delete(id) {
    return UserRepository.delete(id);
  }

  getAll() {
    return UserRepository.getAll();
  }

  search(search) {
    const item = UserRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }
}

module.exports = new UserService();