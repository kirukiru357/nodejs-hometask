const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
  // TODO: Implement methods to work with fighters
  create(data) {
    const search = this.search({'name': data.name});
    if (search)
        throw new Error(`Fighter with name ${data.name} already extists!`);
    return FighterRepository.create(data);
  }

  update(id, dataToUpdate) {
    return FighterRepository.update(id, dataToUpdate);
  }

  delete(id) {
    return FighterRepository.delete(id);
  }

  getAll() {
    return FighterRepository.getAll();
  }

  search(search) {
    const item = FighterRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }
}

module.exports = new FighterService();