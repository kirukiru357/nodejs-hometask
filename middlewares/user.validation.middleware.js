const { stringify } = require('uuid');
const { user } = require('../models/user');
const createUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during creation
  if (req.body.id != undefined) {
    res.status(400).send({error: true, message: `Bad field: id`});
    return;
  }
  for (let field in req.body) {
    if (user[field] === undefined) {
      res.status(400).send({error: true, message: `Bad field: ${field}`});
      return;
    }
  }
  if (!(/\w+@gmail\.com/gi.test(req.body.email))) {
    res.status(400).send({error: true, message: 'Only gmail.com is allowed'});
    return;
  }
  if (!(/^\+380[0-9]{9}$/gi.test(req.body.phoneNumber))) {
    res.status(400).send({error: true, message: 'Wrong phone number format'});
    return;
  }
  if (req.body.password.length < 3) {
    res.status(400).send({error: true, message: 'Password is too short'});
    return;
  }

  res.locals.user = req.body;

  next();
}

const updateUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during update
  if (req.body.id != undefined) {
    res.status(400).send({error: true, message: `Bad field: id`});
    return;
  }
  for (let field in req.body) {
    if (user[field] === undefined) {
      res.status(400).send({error: true, message: `Bad field: ${field}`});
      return;
    }
  }
  if (!(/\w+@gmail\.com/gi.test(req.body.email))) {
    res.status(400).send({error: true, message: 'Only gmail.com is allowed'});
    return;
  }
  if (!(/^\+380[0-9]{9}$/gi.test(req.body.phoneNumber))) {
    res.status(400).send({error: true, message: 'Wrong phone number format'});
    return;
  }
  if (req.body.password.length < 3) {
    res.status(400).send({error: true, message: 'Password is too short'});
    return;
  }

  res.locals.user = req.body;

  next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;