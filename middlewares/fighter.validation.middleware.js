const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
  // TODO: Implement validatior for fighter entity during creation
  if (req.body.id != undefined) {
    res.status(400).send({ error: true, message: `Bad field: id` });
    return;
  }
  for (let field in req.body) {
    if (fighter[field] === undefined) {
      res.status(400).send({ error: true, message: `Bad field: ${field}` });
      return;
    }
  }
  if (!(req.body.power <= 100 && req.body.power > 1)) {
    res.status(400).send({ error: true, message: 'Power is incorrect' });
    return;
  }
  if (!(req.body.defense <= 10 && req.body.defense > 1)) {
    res.status(400).send({ error: true, message: 'Defense is incorrect' });
    return;
  }
  if (!req.body.health || req.body.health === undefined)
    req.body.health == 100;
  if (!(req.body.health <= 120 && req.body.health > 80)) {
    res.status(400).send({ error: true, message: 'Health is incorrect' });
    return;
  }
  if (req.body.name.length < 1) {
    res.status(400).send({ error: true, message: 'Name is too short' });
    return;
  }

  res.locals.fighter = req.body;

  next();
}

const updateFighterValid = (req, res, next) => {
  // TODO: Implement validatior for fighter entity during update
  if (req.body.id != undefined) {
    res.status(400).send({ error: true, message: `Bad field: id` });
    return;
  }
  for (let field in req.body) {
    if (fighter[field] === undefined) {
      res.status(400).send({ error: true, message: `Bad field: ${field}` });
      return;
    }
  }
  if (!(req.body.power <= 100 && req.body.power > 1)) {
    res.status(400).send({ error: true, message: 'Power is incorrect' });
    return;
  }
  if (!(req.body.defense <= 10 && req.body.defense > 1)) {
    res.status(400).send({ error: true, message: 'Defense is incorrect' });
    return;
  }
  if (!req.body.health || req.body.health === undefined)
    req.body.health == 100;
  if (!(req.body.health <= 120 && req.body.health > 80)) {
    res.status(400).send({ error: true, message: 'Health is incorrect' });
    return;
  }
  if (req.body.name.length < 1) {
    res.status(400).send({ error: true, message: 'Name is too short' });
    return;
  }

  res.locals.fighter = req.body;

  next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;