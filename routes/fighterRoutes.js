const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');
const { route } = require('./authRoutes');

const router = Router();

// TODO: Implement route controllers for fighter
router.param('id', function (req, res, next, id) {
  let search = FighterService.search({ 'id': id });
  if (search)
    next();
  else
    res.status(404).send({error: true, message: 'Fighter with given ID doesn\'t exist.'});
    return;
});

router.get('/', (req, res, next) => {
  res.locals.data = FighterService.getAll();
  next();
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
  res.locals.data = FighterService.search({ 'id': req.params.id });
  next();
}, responseMiddleware);

router.post('/', createFighterValid, (req, res, next) => {
  // fighter creation goes here
  try {
    res.locals.data = FighterService.create(res.locals.fighter);
  } catch (err) {
    //console.log(err);
    res.status(400).send({error: true, message: err.message});
  } finally {
    next();
  }
}, responseMiddleware);

router.put('/:id', updateFighterValid, (req, res, next) => {
  try {
    res.locals.data = FighterService.update(req.params.id, res.locals.fighter);
  } catch (err) {
    res.status(400).send({error: true, message: err.message});
  } finally {
    next();
  }
});

router.delete('/:id', (req, res, next) => {
  try {
    res.locals.data = FighterService.delete(req.params.id)
  } catch (err) {
    res.status(400).send({error: true, message: err.message});
  } finally {
    next();
  }
});

module.exports = router;