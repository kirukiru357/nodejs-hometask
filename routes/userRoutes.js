const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user
router.param('id', function (req, res, next, id) {
  let search = UserService.search({ 'id': id });
  if (search)
    next();
  else
    res.status(404).send({error: true, message: 'User with given ID doesn\'t exist.'});
    return;
});

router.get('/', (req, res, next) => {
  res.locals.data = UserService.getAll();
  next();
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
  res.locals.data = UserService.search({ 'id': req.params.id });
  next();
}, responseMiddleware);

router.post('/', createUserValid, (req, res, next) => {
  // registration goes here
  try {
    res.locals.data = UserService.create(res.locals.user);
  } catch (err) {
    //console.log(err);
    res.status(400).send({error: true, message: err.message});
  } finally {
    next();
  }
}, responseMiddleware);

router.put('/:id', updateUserValid, (req, res, next) => {
  try {
    res.locals.data = UserService.update(req.params.id, res.locals.user);
  } catch (err) {
    res.status(400).send({error: true, message: err.message});
  } finally {
    next();
  }
});

router.delete('/:id', (req, res, next) => {
  try {
    res.locals.data = UserService.delete(req.params.id)
  } catch (err) {
    res.status(400).send({error: true, message: err.message});
  } finally {
    next();
  }
});

module.exports = router;